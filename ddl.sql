create database EMS;
use EMS;
create table employee(
e_id int unsigned primary key ,
    e_name varchar(20) not null,
    e_mail varchar(30) unique not null,
    doj datetime not null,
    manager_id int unsigned not null,
    foreign key (manager_id) references employee (e_id)
  );

create table leavedetails(
leaveid int unsigned primary key auto_increment,
    startdate datetime not null,
    enddate datetime not null,
    reason varchar(40) not null,
    e_id int unsigned ,  
    leave_id int unsigned not null,
    leavestatus ENUM('pending','rejected','approved') default 'pending',
    leavetype ENUM('optional leaves','earned leaves','sick leaves'),
   foreign key (e_id) references employee(e_id)
);