package com.hexaware.lms.model;

import java.time.LocalDate;

import static org.junit.Assert.*;
import org.junit.Test;

import com.hexaware.lms.model.LeaveDetails.LeaveStatus;
import com.hexaware.lms.model.LeaveDetails.LeaveType;

public class LeaveDetailsTest {
	LeaveDetails leaveDetails = new LeaveDetails(5000,LocalDate.of(2020, 03, 05),LocalDate.of(2020, 03, 01));

	
	//1
	@Test
	public void testsetId(){
		leaveDetails.setId(2000);		
		assertEquals(2000, leaveDetails.getId());
	}
	
	
	//2
	@Test
	public void testgetEmployee() {
		Employee employee=new Employee("Manjinder", "m@gmail.com", LocalDate.of(2020, 03, 05));
		leaveDetails.setEmployee(employee);		
		assertEquals("Manjinder",leaveDetails.getEmployee().getName());
	}
	
	
	//3
	@Test
	public void testsetReason() {		
		leaveDetails.setReason("sick leave");
		assertEquals("sick leave",leaveDetails.getReason());
	}
	
	
	//4
	@Test
	public void testgetStartDate(){
		leaveDetails.setStartDate(LocalDate.of(2020, 03, 05));		
		assertEquals(LocalDate.of(2020, 03, 05), leaveDetails.getStartDate());
	}
	
	
	//5
	public void testgetEndDate(){
		leaveDetails.setEndDate(LocalDate.of(2020, 03, 02));		
		assertEquals(LocalDate.of(2020, 03, 02), leaveDetails.getStartDate());
	}
	
	
	//6
	@Test
	public void testsetLeaveStatus() {
		leaveDetails.setLeaveStatus(LeaveStatus.Pending);
		assertEquals(LeaveStatus.Pending,leaveDetails.getLeaveStatus());
		
	}
	
	
	//7
	@Test
	public void testgetComments() {
		leaveDetails.setComments("Take care from next time");
		assertEquals("Take care from next time",leaveDetails.getComments());
		
	}
	
	
	//8
	@Test
	public void testsetLeaveType() {
		leaveDetails.setLeaveType(LeaveType.Sickleaves);
		assertEquals(LeaveType.Sickleaves,leaveDetails.getLeaveType());
		
	}
	
	

	

}
