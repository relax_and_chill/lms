package com.hexaware.lms.service;

import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveDetails.LeaveStatus;

public interface LeaveDetailsService {
	
	public LeaveDetails applyforLeave(long empId, LeaveDetails  leaveDetails);
	
	//public Set<LeaveDetails> getLeaveDetails(long empId);
	
	public LeaveDetails update(int leaveId, long managerId, LeaveStatus leaveStatus, String comments);
	
	public int getLeaveBalance(long empId);

	
	
}
