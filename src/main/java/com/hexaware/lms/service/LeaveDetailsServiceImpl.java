package com.hexaware.lms.service;
import com.hexaware.lms.model.*;

import java.util.Set;
import java.time.*;
import java.time.temporal.ChronoUnit;

import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.dao.LeaveDetailsDAOImpl;
import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveDetails.LeaveStatus;
import com.sun.xml.internal.txw2.IllegalAnnotationException;

public class LeaveDetailsServiceImpl implements LeaveDetailsService {
	
	private LeaveDetailsDAO leaveDetailsDAO;



	public LeaveDetailsServiceImpl(LeaveDetailsDAO leaveDetailsDAO2) {
		this.leaveDetailsDAO = leaveDetailsDAO2;
	}

	@Override
	public LeaveDetails applyforLeave(long empId,LeaveDetails leaveDetails) {
		Employee employee=this.leaveDetailsDAO.findByEmpId(empId);
		if(employee == null) {
			throw new IllegalArgumentException("Employee doesnt exist"+empId);
		}
		LocalDate startDate=leaveDetails.getStartDate();
		LocalDate endDate=leaveDetails.getEndDate();
		
		long numberOfDays = ChronoUnit.DAYS.between(startDate,endDate);
		if(numberOfDays > employee.getLeaveBalance()) {
			throw new IllegalArgumentException("Insufficient Leaves");
		}
		//LeaveDetails leave=this.leaveDetailsDAO.save(empId, leaveDetails);
		
		
		return null;
	}

	/*@Override
	public Set<LeaveDetails> getLeaveDetails(long empId) {
		// TODO Auto-generated method stub
		return null;
	}*/

	public LeaveDetails update(int leaveId,long managerId,LeaveStatus leaveStatus,String comments) {

		LeaveDetails leaveDetails=this.leaveDetailsDAO.findByLeaveId(leaveId);
		
		Employee employee=leaveDetails.getEmployee();
		long manID= employee.getManager().getEmpId();
		
		if(manID == managerId) {	
			leaveDetails.setComments(comments);
			leaveDetails.setLeaveStatus(leaveStatus);
		}
		else
		{
			throw new IllegalAnnotationException("invalid managerId");
					
		}
			
		return null;
	}

	@Override
	public int getLeaveBalance(long empId) {
		Employee employee = this.leaveDetailsDAO.findByEmpId(empId);
		if (employee == null) {
			throw new IllegalArgumentException("Invalid employee id");
		}
		return employee.getLeaveBalance();
		
	}

	
	

}
