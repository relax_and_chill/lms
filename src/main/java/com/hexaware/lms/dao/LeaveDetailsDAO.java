package com.hexaware.lms.dao;
import com.hexaware.lms.model.*;
import java.util.*;

public interface LeaveDetailsDAO {
	
	public LeaveDetails save(long empId, LeaveDetails leaveDetails);
	public LeaveDetails save( LeaveDetails leaveDetails);
	
	public LeaveDetails findByLeaveId(long leaveId);
	
	public Employee findByEmpId(long empId);
	
	public LeaveDetails update(long leaveId,LeaveDetails leaveDetails);
	
	public LeaveDetails leaveBalance(LeaveDetails leaveDetails, long empId);
}
