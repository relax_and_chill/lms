package com.hexaware.lms.model;
import java.time.*;

public class LeaveDetails {
	
	private long id;
	private String reason;
	private LocalDate startDate;
	private LocalDate endDate;
	public LeaveStatus leaveStatus;
	public LeaveType leaveType;
	public String comments;
	public Employee employee;
	

	public enum LeaveType{
		Sickleaves,
		Earnedleaves,
		Optionalleaves
	}
	
	public enum LeaveStatus { 
		Approved,
		Pending,
		Rejected
	}
	
	public LeaveDetails(long id,LocalDate startDate,LocalDate endDate)
	{
		this.endDate=endDate;
		this.startDate = startDate;
		this.id =id;
	}
	
	public LeaveDetails(long id, LeaveType leaveType)
	{
		this.id =id;
		this.leaveType=leaveType;
	}
	
	

	//1
	public long getId() {
		return id;
	}
		
	public void setId(long id) {
		this.id = id;
	}

	//2
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	//3
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	

	//4
	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	
	
	//5
	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	
	//6
	public LeaveStatus getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(LeaveStatus leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	
	//7
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	//8
	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((leaveStatus == null) ? 0 : leaveStatus.hashCode());
		result = prime * result + ((leaveType == null) ? 0 : leaveType.hashCode());
		result = prime * result + ((reason == null) ? 0 : reason.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeaveDetails other = (LeaveDetails) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id != other.id)
			return false;
		if (leaveStatus != other.leaveStatus)
			return false;
		if (leaveType != other.leaveType)
			return false;
		if (reason == null) {
			if (other.reason != null)
				return false;
		} else if (!reason.equals(other.reason))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LeaveDetails [id=" + id + ", reason=" + reason + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", leaveStatus=" + leaveStatus + ", leaveType=" + leaveType + "]";
	}

	
	
	  
}
